<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $faker = Faker::create();
         DB::table('users')->insert([
            'name' => $faker->name,
            'email' => $faker->email,
            'fb_url' => 'https://www.facebook.com/'.$faker->word,
            'avatar' => $faker->imageUrl(400, 300, 'cats'),
            'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
            'mobile' => $faker->phoneNumber,
            'status' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);
    }
}
