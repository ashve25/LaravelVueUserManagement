<?php
namespace App\Http\Traits;
use Illuminate\Http\Request;

trait APIResponseTrait
{
	public function error($httpCode, $message, $errorCode){    
    	$response['meta'] = ['http_code' => $httpCode, 'error_code'=>$errorCode, 'message' => $message];    
		$response['data'] = [];   
		return response()->json($response, $httpCode);    
	}  

	public function success( $data = [], $httpCode = 200) {    
		$response['meta'] = ['http_code' => $httpCode,  'error'=>'', 'error_code'=>0];    
		$response['data']  = $data;    
		return $response = response()->json($response, $httpCode);       
	}
}
