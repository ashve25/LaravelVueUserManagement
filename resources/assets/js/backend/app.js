import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import * as Cookies from "js-cookie"
import Toastr from 'vue-toastr'
import 'vue-toastr/dist/vue-toastr.css'
import Snotify, {SnotifyPosition} from 'vue-snotify'

Vue.config.productionTip = false
Vue.use(VueAxios, axios);
Vue.use(Toastr);
Vue.axios.defaults.baseURL = document.head.querySelector('meta[name="admin-base-url"]').content;
Vue.component('vue-toastr',Toastr);

var snotifyOptions = {
	toast: {
		position: SnotifyPosition.rightTop
	}
};

Vue.use(Snotify, snotifyOptions);

router.beforeEach((to, from, next) => {
	next()
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
