import Vue from 'vue'
import Router from 'vue-router'
import UserList from '../components/Users/UserList'
import ViewUser from '../components/Users/ViewUser'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'UserList',
      component: UserList,
      props: true,
      meta: {requiresAuth: false}
	 },
	 {
      path: '/users/:id/',
      name: 'ViewUser',
      component: ViewUser,
      meta: {requiresAuth: false}
    }
  ]
})