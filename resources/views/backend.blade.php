<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="admin-base-url" content="{{ url('/admin') }}/" />
    <meta name="base-url" content="{{ url('/') }}" />
    <meta name="previous-url" content="{{url()->previous().'/'}}" />
    
    <title>Basic | Admin</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/skin-blue.min.css')}}">
    <link rel="stylesheet" href="{{asset('jsgrid/css/jsgrid.css')}}">
    <link rel="stylesheet" href="{{asset('jsgrid/css/theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-datetimepicker.min.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.min.css')}}">
  </head>
  <body class="skin-blue hold-transition sidebar-mini">
    <div id="app"></div>
    <!-- built files will be auto injected -->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/adminlte.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/jsgrid.core.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/jsgrid.load-indicator.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/jsgrid.load-strategies.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/jsgrid.sort-strategies.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/jsgrid.validation.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/jsgrid.field.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/fields/jsgrid.field.text.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/fields/jsgrid.field.checkbox.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/fields/jsgrid.field.control.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/fields/jsgrid.field.number.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/fields/jsgrid.field.select.js')}}"></script>
    <script type="text/javascript" src="{{asset('jsgrid/src/fields/jsgrid.field.textarea.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/backend.js')}}"></script>
    <script type="text/javascript">
        var DATE_FORMAT = 'YYYY-MM-DD';

  var DateField = function(config) {
      jsGrid.Field.call(this, config);
    };
     
    DateField.prototype = new jsGrid.Field({
     
        css: "date-field",            // redefine general property 'css'
        
        myCustomProperty: "foo",      // custom property
     
        itemTemplate: function(value) {
          if(value == null || value == '') return "";
          return moment(value, DATE_FORMAT).format(DATE_FORMAT);
        },
     
        insertTemplate: function(value) {
          return this._insertPicker = $("<input>").datetimepicker({format: DATE_FORMAT});
        },
     
        editTemplate: function(value) {
          var template = $("<input>").datetimepicker({format: DATE_FORMAT});
          if(value != null) {
            var date = moment(value, DATE_FORMAT);
            template.data("DateTimePicker").date(date);
          }
          return this._editPicker = template;
        },
     
        insertValue: function() {
          var date = this._insertPicker.data("DateTimePicker").date();
          if(date == null) return '';
          return date.format(DATE_FORMAT);
        },
     
        editValue: function() {
          var date = this._editPicker.data("DateTimePicker").date();
          if(date == null) return '';
          return date.format(DATE_FORMAT);
        },

        filterTemplate: function() {
          var grid = this._grid;
          this._filterDatePicker = $("<input>").datetimepicker({
              format: DATE_FORMAT, showClose: true, showClear: true
            });
          this._filterDatePicker.on('dp.hide', function(e){
            grid.search();
          });
          return this._filterDatePicker;
        },

        filterValue: function() {
          var date = this._filterDatePicker.data("DateTimePicker").date();
          if(date == null) return '';
          return date.format(DATE_FORMAT);
        }
    });
   
    jsGrid.fields.date = DateField;
    </script>
  </body>
</html>
